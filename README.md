# [Caldera Forms](https://CalderaForms.com] Submission Comparison

[See the WIP WordPress plugin that uses this](https://gitlab.com/caldera-labs/cf-result-diff)

### Install
```
{
    "require": {
        "calderawp/cf-result-diff": "*"
    },
    "repositories" : [
        {
            "type": "git",
            "url" : "git@gitlab.com:caldera-labs/cf-result-diff.git"
        }
    ]
}