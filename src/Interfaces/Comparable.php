<?php

namespace calderawp\testing\resultDiff\Interfaces;


use calderawp\interop\Interfaces\Arrayable;
use calderawp\testing\resultDiff\DB\Entity;

interface Comparable extends Arrayable
{

    /**
     * @return string
     */
    public function getHash();

    /**
     * Create an storable Entity from object
     *
     * @return Entity
     *
     * @throws \Exception If form property is not set
     */
    public function toEntity();
}