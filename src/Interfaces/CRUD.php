<?php


namespace calderawp\testing\resultDiff\Interfaces;


use calderawp\testing\resultDiff\DB\Entity;

/**
 * Interface CRUD
 *
 * Interface that DB interactions MUST implement.
 *
 * @package calderawp\testing\resultDiff\Interfaces
 */
interface CRUD
{

    /**
     * Store new Entity
     *
     * @param Entity $entity Entity to store
     *
     * @return Entity
     */
    public function create( Entity $entity );

    /**
     * Get stored Entity by ID
     *
     * @param string $ID ID of entity
     * @return Entity
     */
    public function read( $ID );

    /**
     * Update entity
     *
     * @param Entity $entity Entity to update
     *
     * @return bool True if deleted
     */
    public function update( Entity $entity );

    /**
     * Delete stores Entity
     *
     * @param Entity $entity Entity to delete
     *
     * @return bool True if deleted
     */
    public function delete( Entity $entity );

}