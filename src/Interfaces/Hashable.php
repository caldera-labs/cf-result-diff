<?php


namespace calderawp\testing\resultDiff\Interfaces;

/**
 * Interface Hashable
 *
 * Interface that objects that CAN be represented as a hash MUST impliment
 *
 * @package calderawp\testing\resultDiff\Interfaces
 */
interface Hashable
{

    /**
     * Get hash representation of object
     *
     * @return string
     */
    public function getHash();

}