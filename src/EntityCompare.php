<?php


namespace calderawp\testing\resultDiff;


use calderawp\calderaforms\pro\exceptions\Exception;
use calderawp\testing\resultDiff\DB\Entity;

class EntityCompare
{

    /** @var  Entity */
    protected $entityOne;

    /** @var  Entity */
    protected $entityTwo;


    public function __construct( Entity $entityOne, Entity $entityTwo )
    {
        $this->entityOne = $entityOne;
        $this->entityTwo = $entityTwo;

    }

    /**
     * @return bool
     */
    public function validate()
    {
        $this->validateForm();
        return $this->entityOne->getHash() === $this->entityTwo->getHash();
    }

    /**
     * Ensure that
     *
     * @return bool
     * @throws \Exception
     */
    public function validateForm(){
        if( ! $this->formIdEquals() ){
            throw new \Exception( 'Different form Id' );
        }elseif( ! $this->formIdEquals() ){
            throw new \Exception( 'Different form config' );
        }

        return true;
    }

    /**
     * Check that form hash is the same
     *
     * @return bool
     */
    protected function formEquals()
    {
        return $this->entityOne->getFormHash() === $this->entityTwo->getFormHash();
    }

    /**
     * Check that form ID is the same
     */
    protected function formIdEquals()
    {
        return $this->entityOne->getFormId() === $this->entityTwo->getFormId();
    }

}