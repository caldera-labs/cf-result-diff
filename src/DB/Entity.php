<?php


namespace calderawp\testing\resultDiff\DB;


use calderawp\testing\resultDiff\Interfaces\Hashable;


/**
 * Class Entity
 *
 * Object representation of object stored as identified hash
 *
 * @package calderawp\testing\resultDiff\DB
 */
class Entity extends \calderawp\interop\Entities\Entity implements Hashable
{

    protected $ID;
    protected $type;
    protected $hash;
    protected $formHash;
    protected $formId;
    protected $reference;


    /**
     * Entity constructor.
     *
     *
     * @param array $rawData
     */
    public function __construct( array  $rawData )
    {
        foreach ( $rawData as $key => $datum ){
            $setter = 'set' . ucfirst( $key );
            if( method_exists( $this, $setter ) ){
                $this->$setter( $datum );
            }else if( property_exists( $this, $key ) ){
                $this->$key = $datum;
            }

        }
    }

    /**
     * @inheritdoc
     */
    public function setId( $id )
    {
        $this->ID = $id;
        $this->id = $id;
        return $this;
    }


    /** @inheritdoc */
    public function __get($name)
    {
        $getter = 'get' . ucfirst($name);
        if ( method_exists($this, $getter ) ) {
            return $this->$getter();
        }

        if (property_exists($this, $name)) {
            return $this->$name;
        }

    }

    /** @inheritdoc */
    public function toArray()
    {
        return [
            'ID'                => $this->getId(),
            'type'              => $this->type,
            'hash'              => $this->getHash(),
            'formHash'          => $this->getFormHash(),
            'formId'            => $this->getFormId(),
            'reference'         => $this->getReference()
        ];
    }

    /** @inheritdoc */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Get form object hash
     *
     * @return string
     */
    public function getFormHash()
    {
        return $this->formHash;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Get form ID
     *
     * @return string
     */
    public function getFormId()
    {
        return $this->formId;
    }

    /**
     * Get if this is a reference save.
     *
     * @return bool|string
     */
    public function getReference()
    {
        if( empty( $this->reference ) || true !== $this->reference || ! is_string( $this->reference ) ){
            return false;
        }

        return $this->reference;
    }
}