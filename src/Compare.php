<?php

namespace calderawp\testing\resultDiff;


use calderawp\testing\resultDiff\Interfaces\Comparable;


/**
 * Class Compare
 *
 * Compare any two `Comparable` objects
 *
 * @package calderawp\testing\resultDiff
 */
class Compare
{

    /**
     * @var Comparable
     */
    protected $comparableOne;

    /**
     * @var Comparable
     */
    protected $comparableTwo;

    /**
     * Compare constructor.
     *
     * @param Comparable $comparableOne
     * @param Comparable $comparableTwo
     */
    public function __construct( Comparable $comparableOne, Comparable $comparableTwo )
    {
        $this->comparableOne = $comparableOne;
        $this->comparableTwo = $comparableTwo;
    }

    /**
     * Check if the two objects are equals
     *
     * @return bool
     * @throws Exception
     */
    public function equals()
    {
        if( $this->comparableOne->toEntity()->getType() !== $this->comparableTwo->toEntity()->getType()  ){
            throw new Exception();
        }
        //NOTE: This is not a nonce-type hash, so not using hash_equals, which is *intentionally* slower.
        return $this->comparableOne->getHash() === $this->comparableTwo->getHash();
    }

}