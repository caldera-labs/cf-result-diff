<?php

namespace calderawp\testing\resultDiff\Traits;


trait Hashes
{


    public function create( $algo, $data )
    {
        return hash( $algo, $data );
    }

    abstract public function getHash();

    public function __toString()
    {
        return $this->getHash();
    }
}