<?php


namespace calderawp\testing\resultDiff\Traits;

/**
 * Trait MockData
 *
 * Mock data, mainly for unit/integration testing, but also could seed tests.
 *
 * @package calderawp\testing\resultDiff\Traits
 */
trait MockData

{

    /**
     * Get mock form
     *
     * @return \calderawp\testing\resultDiff\ArrayLike\Form
     */
    public function getMockForm()
    {

        return $form = new \calderawp\testing\resultDiff\ArrayLike\Form( $this->mockData( 'Form' ) );

    }

    /**
     * Get an array of mock data
     *
     * @param string $type
     * @param  array $defaultData Optional. Overwrite data by key
     * @return array
     */
    public function mockData( $type, array  $defaultData = [] )
    {
        switch ( $type ){
            case 'caldera_forms_mailer':
                $data = array (
                    'recipients' =>
                        array (
                            0 => 'f@nopm.com',
                        ),
                    'subject' => 'XC',
                    'message' => '<strong>First Name</strong><div style="margin-bottom:20px;">Josh</div>
<strong>Email Address</strong><div style="margin-bottom:20px;">josh@joshpress.net</div>
<strong>Comments / Questions</strong><div style="margin-bottom:20px;">Hi</div>
',
                    'headers' =>
                        array (
                            0 => 'From: Caldera Forms Notification <f@nopm.com>',
                            1 => 'Reply-To: ',
                            2 => 'Content-type: text/html',
                        ),
                    'attachments' =>
                        array (
                        ),
                    'from' => 'f@nopm.com',
                    'from_name' => 'Caldera Forms Notification',
                    'bcc' => false,
                    'replyto' => 'f@nopm.com',
                    'html' => true,
                    'csv' => false,
                );
                break;
            case 'EmailData':
                $data = [
                    'fld_29462' => NULL,
                    'fld_8768091' => 'Josh',
                    'fld_9970286' => 'Pollock',
                    'fld_6009157' => 'josh@calderawp.com',
                    'fld_2758980' => NULL,
                    'fld_7683514' => "Hi thanks for being a form that I can use on the internet. \\n\\n Hi Roy",
                    'fld_7908577' => 'click',
                    '_entry_id' => rand( 1,100000 ), //this ensures that _entry_id is not tracked, since it should be different every time.
                    '_entry_token' => sha1( random_bytes(rand(42,84) ) ), //this ensures that _entry_token is not tracked, since it should be different every time.
                ];
                break;
            case 'EmailSettings' :
                $form = $this->mockData( 'Form' );
                $data = $form[ 'mailer' ];
                break;
            case 'Form' :
                $data =             array (
                    'name' => 'Contact',
                    'description' => '',
                    'db_support' => 1,
                    'hide_form' => 1,
                    'success' => 'Form has been successfully submitted. Thank you.',
                    'avatar_field' => 'fld_6009157',
                    'form_ajax' => 1,
                    'layout_grid' =>
                        array (
                            'fields' =>
                                array (
                                    'fld_29462' => '1:1',
                                    'fld_8768091' => '2:1',
                                    'fld_9970286' => '2:2',
                                    'fld_6009157' => '2:3',
                                    'fld_2758980' => '3:1',
                                    'fld_7683514' => '4:1',
                                    'fld_7908577' => '5:1',
                                ),
                            'structure' => '12|4:4:4|12|12|12',
                        ),
                    'fields' =>
                        array (
                            'fld_29462' =>
                                array (
                                    'ID' => 'fld_29462',
                                    'type' => 'html',
                                    'label' => 'header',
                                    'slug' => 'header',
                                    'caption' => '',
                                    'config' =>
                                        array (
                                            'custom_class' => '',
                                            'default' => '<h2>Your Details</h2>
<p>Let us know how to get back to you.</p>
<hr>',
                                        ),
                                    'conditions' =>
                                        array (
                                            'type' => '',
                                        ),
                                ),
                            'fld_8768091' =>
                                array (
                                    'ID' => 'fld_8768091',
                                    'type' => 'text',
                                    'label' => 'First Name',
                                    'slug' => 'first_name',
                                    'required' => '1',
                                    'caption' => '',
                                    'config' =>
                                        array (
                                            'custom_class' => '',
                                            'placeholder' => '',
                                            'default' => '',
                                            'mask' => '',
                                        ),
                                    'conditions' =>
                                        array (
                                            'type' => '',
                                        ),
                                ),
                            'fld_9970286' =>
                                array (
                                    'ID' => 'fld_9970286',
                                    'type' => 'text',
                                    'label' => 'Last Name',
                                    'slug' => 'last_name',
                                    'required' => '1',
                                    'caption' => '',
                                    'config' =>
                                        array (
                                            'custom_class' => '',
                                            'placeholder' => '',
                                            'default' => '',
                                            'mask' => '',
                                        ),
                                    'conditions' =>
                                        array (
                                            'type' => '',
                                        ),
                                ),
                            'fld_6009157' =>
                                array (
                                    'ID' => 'fld_6009157',
                                    'type' => 'email',
                                    'label' => 'Email Address',
                                    'slug' => 'email_address',
                                    'required' => '1',
                                    'caption' => '',
                                    'config' =>
                                        array (
                                            'custom_class' => '',
                                            'placeholder' => '',
                                            'default' => '',
                                        ),
                                    'conditions' =>
                                        array (
                                            'type' => '',
                                        ),
                                ),
                            'fld_2758980' =>
                                array (
                                    'ID' => 'fld_2758980',
                                    'type' => 'html',
                                    'label' => 'Message',
                                    'slug' => 'message',
                                    'caption' => '',
                                    'config' =>
                                        array (
                                            'custom_class' => '',
                                            'default' => '<h2>How can we help?</h2>
<p>Feel free to ask a question or simply leave a comment.</p>
<hr>',
                                        ),
                                    'conditions' =>
                                        array (
                                            'type' => '',
                                        ),
                                ),
                            'fld_7683514' =>
                                array (
                                    'ID' => 'fld_7683514',
                                    'type' => 'paragraph',
                                    'label' => 'Comments / Questions',
                                    'slug' => 'comments_questions',
                                    'required' => '1',
                                    'caption' => '',
                                    'config' =>
                                        array (
                                            'custom_class' => '',
                                            'placeholder' => '',
                                            'rows' => '7',
                                            'default' => '',
                                        ),
                                    'conditions' =>
                                        array (
                                            'type' => '',
                                        ),
                                ),
                            'fld_7908577' =>
                                array (
                                    'ID' => 'fld_7908577',
                                    'type' => 'button',
                                    'label' => 'Send Message',
                                    'slug' => 'submit',
                                    'caption' => '',
                                    'config' =>
                                        array (
                                            'custom_class' => '',
                                            'type' => 'submit',
                                            'class' => 'btn btn-default',
                                        ),
                                    'conditions' =>
                                        array (
                                            'type' => '',
                                        ),
                                ),
                        ),
                    'page_names' =>
                        array (
                            0 => 'Page 1',
                        ),
                    'processors' =>
                        array (
                            'fp_17689566' =>
                                array (
                                    'ID' => 'fp_17689566',
                                    'type' => 'auto_responder',
                                    'config' =>
                                        array (
                                            'sender_name' => 'tuxDuchamps',
                                            'sender_email' => 'dev-email@flywheel.local',
                                            'subject' => 'Thank you for contacting us',
                                            'recipient_name' => '%first_name% %last_name%',
                                            'recipient_email' => '%email_address%',
                                            'message' => 'Hi %recipient_name%.
Thanks for your email.
We\'ll get back to you as soon as possible!
Here\'s a summary of your message:
------------------------
{summary}',
                                        ),
                                    'conditions' =>
                                        array (
                                            'type' => '',
                                        ),
                                ),
                        ),
                    'settings' =>
                        array (
                            'responsive' =>
                                array (
                                    'break_point' => 'sm',
                                ),
                        ),
                    'mailer' =>
                        array (
                            'on_insert' => 1,
                            'email_message' => '{summary}',
                            'email_subject' => 'Contact',
                        ),
                    'ID' => 'CF5a0dbafe5f120',
                    'check_honey' => 1,
                    'template' => 'starter_contact_form',
                    'db_id' => '27',
                    'type' => 'primary',
                );
                break;
            case 'Entity' :
                $data = [
                    'ID'            => rand(),
                    'type'          => 'EmailData',
                    'hash'          => md5( random_bytes( rand( 42,48 ) ) ),
                    'formHash'      => md5( random_bytes( rand( 42,48 ) ) ),
                    'formId'        => uniqid( 'CF' )
                ];
                break;
            default :
                $data = [];
                break;

        }

        return array_merge( $data, $defaultData );

    }

    /**
     * @param string $type
     * @return \calderawp\testing\resultDiff\DB\Entity
     */
    protected function getMockDBEntity( $type, $formId, $entryId )
    {
        $entity = new \calderawp\testing\resultDiff\DB\Entity(
            $this->mockData( 'Entity', [ 'type' => $type ] )
        );

        $entity->entry_id = $entryId;
        $entity->formId = $formId;
        return $entity;
    }

    /**
     * @param array $mockData
     * @return \calderawp\testing\resultDiff\Comparisons\EmailData
     */
    public function getEmailDataObject( array $mockData = [] )
    {
        return new \calderawp\testing\resultDiff\Comparisons\EmailData(
            $this->mockData( 'EmailData', $mockData  ),
            $this->getMockForm()
        );
    }

    /**
     * @return \calderawp\interop\Entities\Entry
     */
    protected function getMockEntryEntity()
    {
        $app = \calderawp\interop\Interop();
        $entryEntity = $app->createEntity(
            \calderawp\interop\Entities\Entry::class
        );
        return $entryEntity;
    }


}