<?php


namespace calderawp\testing\resultDiff\Traits;

/**
 * Trait HashesToArray
 *
 * Provides array hashing (non-recursive)
 *
 * @package calderawp\testing\resultDiff\Traits
 */
trait HashesToArray
{
    use Hashes;

    /**
     * @return array
     */
    abstract public function toArray();

    /**
     * Get md5 of json_serialized array
     *
     * @return string
     */
    public function getHash()
    {
       return md5( json_encode( $this->toArray() ) );
    }


}