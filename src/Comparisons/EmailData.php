<?php

namespace calderawp\testing\resultDiff\Comparisons;

use calderawp\testing\resultDiff\ArrayLike\Form;

/**
 * Class EmailData
 *
 * This object describes the array used in \Caldera_Forms_Save_Final::mailer() to construct email data. More specifically, it's structure when the caldera_forms_mailer filter is reached.
 *
 * This is assumed to be useful, since changes in this array *should* reflect a problem with entry processing, internal APIs for data, magic tags and \Caldera_Forms_Save_Final::mailer(). These issues manifest as bad email content or headings, and issues with entry viewer.
 *
 * TL;DR Given a change to what we are comparing here, we can assume a problem is happening somewhere critical.
 *
 * @package calderawp\testing\resultDiff
 */
class EmailData extends Comparison
{

    /**
     * @var Form
     */
    protected $form;


    /**
     * EmailData constructor.
     *
     * @param array $data Array from the caldera_forms_mailer filter
     * @param Form $form Form config
     */
    public function __construct( array  $data, Form  $form )
    {

        $this->data = $data;
        $this->form = $form;
        $this->badKeys = [
            '_entry_token',
            '_entry_id'
        ];

    }



}

