<?php


namespace calderawp\testing\resultDiff\Comparisons;
use \calderawp\testing\resultDiff\ArrayLike\Form;
use \calderawp\interop\Entities\Entry as EntryEntity;
class Entry extends Comparison
{

    /**
     * @var EntryEntity
     */
    protected $entry;

    /**
     * Entry constructor.
     * @param EntryEntity $entry
     * @param Form $form
     */
    public function __construct(  EntryEntity $entry, Form $form )
    {
        $this->form = $form;
        $this->entry = $entry;

    }

    /** @inheritdoc */
    public function prepareData()
    {
        return $this->entry->toArray();

    }
}