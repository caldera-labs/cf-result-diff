<?php


namespace calderawp\testing\resultDiff\Comparisons;

/**
 * Class EmailArgs
 *
 * Test that $mail argument passed by caldera_forms_mailer is the same. Cover headers, subject, content and CSV
 *
 *
 * @package calderawp\testing\resultDiff\Comparisons
 */
class EmailArgs extends Comparison
{

    /**
     * EmailArgs constructor.
     *
     * @param array $mail First param of caldera_forms_mailer
     * @param Form $form Form config
     */
    public function __construct( array $mail, \calderawp\testing\resultDiff\ArrayLike\Form $form )
    {
        $this->data = $mail;
        $this->form = $form;
    }

}