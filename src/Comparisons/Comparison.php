<?php


namespace calderawp\testing\resultDiff\Comparisons;

use calderawp\testing\resultDiff\DB\Entity;
use calderawp\testing\resultDiff\Interfaces\Comparable;
use calderawp\testing\resultDiff\Interfaces\Hashable;
use calderawp\testing\resultDiff\Traits\HashesToArray;
use calderawp\testing\resultDiff\Comparisons\Form as FormCompare;
use calderawp\testing\resultDiff\ArrayLike\Form as FormArrayLike;

abstract class Comparison implements  Comparable, Hashable
{
    use  HashesToArray;

    /**
     * Data for comparison
     *
     * @var array
     */
    protected  $data;

    /**
     * Blacklisted array keys
     *
     * @var array
     */
    protected $badKeys;

    /**
     * @var FormArrayLike
     */
    protected $form;


    /** @inheritdoc */
    public function toArray()
    {
        return $this->prepareData();
    }

    /**
     * Create an storable Entity from object
     *
     * @return Entity
     *
     * @throws \Exception If form property is not set
     */
    public function toEntity()
    {
        if( ! $this->form ){
            throw new \Exception(
                'Must set Form first'
            );
        }

        return new Entity(
            [
                'type' => $this->getType(),
                'hash' => $this->getHash(),
                'formHash' => $this->getFormHash(),
                'formId' => $this->form[ 'ID' ]
            ]
        );
    }

    /**
     * Get form hash
     *
     * @return string
     */
    protected function getFormHash()
    {
        if( 'Form' == $this->getType() ) {
            return $this->getHash();
        }

        return ( new FormCompare( $this->form ) )->getHash();


    }

    /**
     * (re)set Form property
     *
     * @param FormArrayLike $form
     * @return $this
     */
    public function setForm ( \calderawp\testing\resultDiff\ArrayLike\Form $form )
    {
        $this->form = $form;
        return $this;
    }

    /**
     * Prepare data for comparison
     *
     * @return array
     */
    public function prepareData()
    {
        if( ! is_array( $this->data ) || empty( $this->data ) )
        {
            $this->data = [];
        }

        if( ! is_array( $this->badKeys ) || empty( $this->badKeys ) )
        {
            return $this->data;
        }


        return array_diff_key( $this->data, array_flip( $this->badKeys ) );
    }

    /**
     * Get comparison type
     *
     * Effectively the subclass' unqualified name
     *
     * @return string
     */
    public function getType()
    {
        return substr( strrchr( get_class($this ), '\\'), 1 );
    }
}