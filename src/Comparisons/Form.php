<?php


namespace calderawp\testing\resultDiff\Comparisons;

use calderawp\testing\resultDiff\ArrayLike\Form as FormArrayLike;
/**
 * Class Form
 *
 * Creates an object hash of Form config
 *
 * Mainly putting this here to ensure that that the comparison isn't done with different form configs.
 *
 * @package calderawp\testing\resultDiff\Comparisons
 */
class Form extends Comparison
{
    /**
     * @var FormArrayLike
     */
    protected $form;

    /**
     * Form constructor.
     * @param \calderawp\testing\resultDiff\ArrayLike\Form  $form
     */
    public function __construct( FormArrayLike $form )
    {
        $this->form = $form;
        $this->badKeys = [
            'db_id',
        ];

    }

    /** @inheritdoc */
    public function prepareData()
    {
        $this->data = $this->form->toArray();
        return parent::prepareData();
    }

}