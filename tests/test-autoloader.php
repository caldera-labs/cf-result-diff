<?php
include_once dirname( dirname( __FILE__ ) ) . '/vendor/autoload.php';
spl_autoload_register( function( $class ){
    $path = __DIR__ . '/' . $class . '.php';
    if( file_exists( $path ) ){
        include_once $path;
    }
});
