<?php


class EmailArgsCompareTest extends CompareTestCase
{

    /**
     * Test prepare data for this comparison
     *
     * @covers Comparison::toArray()
     * @covers Comparison::prepareData()
     * @covers EmailArgs::toArray()
     * @covers EmailArgs::prepareData()
     * @covers EmailArgs::__construct()
     */
    public function testPrepareData()
    {
        $mockForm = $this->getMockForm();
        $mockData = $this->mockData( 'caldera_forms_mailer' );
        $comparisonOne = new \calderawp\testing\resultDiff\Comparisons\EmailArgs(
            $mockData,
            $mockForm
        );

        $this->assertEquals(
            $mockData,
            $comparisonOne->prepareData()
        );
        $this->assertEquals(
            $mockData,
            $comparisonOne->toArray()
        );

    }

    /**
     * Test create hash is consistent with this data
     *
     * @covers Comparison::getHash()
     * @covers EmailArgs::getHash()
     */
    public function testHashEquality()
    {
        $mockForm = $this->getMockForm();
        $comparisonOne = new \calderawp\testing\resultDiff\Comparisons\EmailArgs(
            $this->mockData( 'caldera_forms_mailer' ),
            $mockForm
        );

        $comparisonTwo = new \calderawp\testing\resultDiff\Comparisons\EmailArgs(
            $this->mockData( 'caldera_forms_mailer' ),
            $mockForm
        );

        $this->assertEquals(
            $comparisonOne->getHash(),
            $comparisonTwo->getHash()
        );


    }

    /**
     * Test create hash is creates different hashes from different data
     *
     * @covers Comparison::getHash()
     * @covers EmailArgs::getHash()
     */
    public function testHashInequality()
    {
        $mockForm = $this->getMockForm();
        $comparisonOne = new \calderawp\testing\resultDiff\Comparisons\EmailArgs(
            $this->mockData( 'caldera_forms_mailer' ),
            $mockForm
        );

        $data2 = $this->mockData( 'caldera_forms_mailer' );
        $data2[ 'recipients' ] = array (
            0 => 'hi@roysivan.com',
        );

        $comparisonTwo = new \calderawp\testing\resultDiff\Comparisons\EmailArgs(
            $data2,
            $mockForm
        );

        $this->assertNotEquals(
            $comparisonOne->getHash(),
            $comparisonTwo->getHash()
        );


    }

    /**
     * Ensures that two identical objects are considered equal
     *
     * @covers EmailArgs::getHash()
     * @covers Comparison::getHash()
     * @covers Compare::equals()
     */
    public function testCompareEquals()
    {
        $mockForm = $this->getMockForm();
        $comparisonOne = new \calderawp\testing\resultDiff\Comparisons\EmailArgs(
            $this->mockData( 'caldera_forms_mailer' ),
            $mockForm
        );

        $comparisonTwo = new \calderawp\testing\resultDiff\Comparisons\EmailArgs(
            $this->mockData( 'caldera_forms_mailer' ),
            $mockForm
        );

        $compare = new \calderawp\testing\resultDiff\Compare( $comparisonOne, $comparisonTwo );
        $this->assertTrue( $compare->equals() );

    }

    /**
     * Ensures that two different objects are not considered equal
     *
     * @covers EmailArgs::getHash()
     * @covers Comparison::getHash()
     * @covers Compare::equals()
     */
    public function testCompareNotEquals()
    {

        $mockForm = $this->getMockForm();
        $comparisonOne = new \calderawp\testing\resultDiff\Comparisons\EmailArgs(
            $this->mockData( 'caldera_forms_mailer' ),
            $mockForm
        );

        $data2 = $this->mockData( 'caldera_forms_mailer' );
        $data2[ 'recipients' ] = array (
            0 => 'hi@roysivan.com',
        );

        $comparisonTwo = new \calderawp\testing\resultDiff\Comparisons\EmailArgs(
            $data2,
            $mockForm
        );

        $compare = new \calderawp\testing\resultDiff\Compare( $comparisonOne, $comparisonTwo );
        $this->assertFalse( $compare->equals() );


    }
}