<?php


class CompareTest extends CompareTestCase
{

    /**
     * Test that the same two items are considered equal
     *
     * @covers \calderawp\testing\resultDiff\Compare::equals()
     */
    public function testComparisionSame()
    {
        $obj = $this->getCompareObject( 'EmailData' );
        $this->assertTrue( $obj->equals() );

    }

    /**
     * Test that two different items are not considered equals
     *
     * @covers \calderawp\testing\resultDiff\Compare::equals()
     */
    public function testComparisionNotSame()
    {
        $obj =  new \calderawp\testing\resultDiff\Compare(
            $this->getEmailDataObject(),
            $this->getEmailDataObject( [ 'fld_9970286' => rand(42, 84 ) ] )
        );

        $this->assertFalse( $obj->equals() );
    }

    /**
     * @param string $type
     * @return \calderawp\testing\resultDiff\Compare
     */
    public function getCompareObject( $type = 'EmailData' )
    {

        switch( $type )
        {
            case  'EmailData' :
                return new \calderawp\testing\resultDiff\Compare(
                    $this->getEmailDataObject(),
                    $this->getEmailDataObject()
                );
            break;
        }
    }


}