<?php

/**
 * Class EmailDataCompareTest
 *
 * @covers \calderawp\testing\resultDiff\Comparisons\EmailData
 */
class EmailDataCompareTest extends CompareTestCase
{

    /**
     * Test that mock data is valid
     *
     * @covers EmailDataCompareTest::getEmailMockData()
     */
    public function testEmailMockData()
    {
        $this->assertFalse( empty( $this->getEmailMockData() ) );
    }

    /**
     * Test that prepareData removes keys we don't want to test
     *
     * @covers \calderawp\testing\resultDiff\Comparisons\EmailData::prepareData()
     */
    public function testPrepareData()
    {
        $expected = $this->getEmailMockData();
        unset( $expected[ '_entry_token' ] );
        unset( $expected[ '_entry_id' ] );

        $this->assertEquals(
            $expected,
            $this->getMockEmail()->prepareData()
        );

    }

    /**
     * Test that prepareData and toArray produce same result
     *
     * @covers \calderawp\testing\resultDiff\Comparisons\EmailData::toArray()
     */
    public function testToArray()
    {
        $this->assertEquals(
            $this->getMockEmail()->toArray(),
            $this->getMockEmail()->prepareData()
        );

    }


    /**
     * Test that two identical objects produce the same hash
     *
     * @covers \calderawp\testing\resultDiff\Comparisons\EmailData::prepareData()
     * @covers \calderawp\testing\resultDiff\Comparisons\EmailData::getHash()
     * @covers \calderawp\testing\resultDiff\Traits\HashesToArray::getHash()
     */
    public function testHashEquality()
    {
        $this->assertEquals(
            $this->getMockEmail()->getHash(),
            $this->getMockEmail()->getHash()
        );
    }

    /**
     * Test that type is 'EmailData'
     *
     * @covers \calderawp\testing\resultDiff\Comparisons\Comparison::getType()
     * @covers \calderawp\testing\resultDiff\Comparisons\EmailData::getType()
     */
    public function testGetType()
    {
        $this->assertEquals( 'EmailData', $this->getMockEmail()->getType() );
    }

    /**
     * Test entity conversion
     *
     * @covers \calderawp\testing\resultDiff\Comparisons\Comparison::toEntity()
     * @covers \calderawp\testing\resultDiff\Comparisons\EmailData::toEntity()
     */
    public function testToEntity()
    {
        $mockForm = $this->getMockForm();
        $formComparison = new \calderawp\testing\resultDiff\Comparisons\Form( $mockForm );

        $emailDataComparison =  new \calderawp\testing\resultDiff\Comparisons\EmailData(
            $this->mockData( 'EmailData' ),
            $mockForm
        );

        $mockEntityData = $this->mockData( 'Entity', [
            'formHash'  => $formComparison->getHash(),
            'formId'    => $mockForm[ 'ID' ],
            'type'      => 'EmailData',
            'hash'      => $emailDataComparison->getHash()
        ]);

        $entity = new \calderawp\testing\resultDiff\DB\Entity(
            $mockEntityData
        );

        $entity->setID( $mockEntityData[ 'ID' ] );

        $resultEntity = $emailDataComparison->toEntity()->setID( $mockEntityData[ 'ID' ] );
        $this->assertEquals(
            $entity,
            $resultEntity

        );
    }

    /**
     * Test that the same object is considered equals
     *
     * @covers Compare::equals()
     */
    public function testCompare()
    {
        $mockForm = $this->getMockForm();

        $emailDataComparison =  new \calderawp\testing\resultDiff\Comparisons\EmailData(
            $this->mockData( 'EmailData' ),
            $mockForm
        );

        $compare = new \calderawp\testing\resultDiff\Compare( $emailDataComparison, $emailDataComparison );
        $this->assertTrue( $compare->equals() );

    }

    /**
     * @return \calderawp\testing\resultDiff\Comparisons\EmailData
     */
    protected function getMockEmail()
    {
        return new \calderawp\testing\resultDiff\Comparisons\EmailData(
            $this->getEmailMockData(),
            $this->getMockForm()
        );

    }

    /**
     * @return array
     */
    protected function getEmailMockData()
    {
        return $this->mockData( 'EmailData' );
    }



}