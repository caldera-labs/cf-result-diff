<?php


class EntityTest extends CompareTestCase
{

    /**
     * Make sure mock data isn't empty
     *
     *
     * @covers EntityTest::getMockData()
     */
    public function testMockData()
    {
        $this->assertInternalType(
            'array',
            $this->getMockData()
        );

        $this->assertFalse(
            empty( $this->getMockData() )
        );
    }

    /**
     *
     * @covers \calderawp\testing\resultDiff\DB\Entity::__construct()
     * @covers \calderawp\testing\resultDiff\DB\Entity::toArray()
     */
    public function testCreate()
    {
        $mock = $this->getMockData();
        $obj = new \calderawp\testing\resultDiff\DB\Entity(
            $mock
        );

        $array = $obj->toArray();
        foreach( $mock as $key => $value )
        {
            $this->assertArrayHasKey( $key, $array );
            $this->assertEquals( $mock[ $key ], $array[ $key ], $key );
            $this->assertEquals( $mock[ $key ], $obj->$key, $key );
        }
    }


    protected function getMockData()
    {
        return $this->mockData('Entity' );
    }

}