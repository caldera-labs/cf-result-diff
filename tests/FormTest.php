<?php


class FormTest extends CompareTestCase
{


    /**
     * Test mock form array data isn't empty
     *
     *
     * @covers FormTest::getMockData();
     * @covers CompareTestCase::mockData()
     */
    public function testMockData()
    {
        $this->assertFalse( empty( $this->getMockData() ) );
    }

    /**
     * Test that prepareData removes keys we don't want to test
     *
     * @covers \calderawp\testing\resultDiff\Comparisons\Form::prepareData()
     */
    public function testPrepareData()
    {
        $expected = $this->getMockData();
        unset( $expected[ 'db_id' ] );

        $this->assertEquals(
            $expected,
            $this->getMockObject()->prepareData()
        );
    }

    /**
     * Get mock form data config array
     *
     * @return array
     */
    public function getMockData()
    {
        return $this->mockData( 'Form' );
    }

    /**
     * Get mock compare object for testing
     *
     * @return \calderawp\testing\resultDiff\Comparisons\Form
     */
    public function getMockObject()
    {
        return new \calderawp\testing\resultDiff\Comparisons\Form(
             new \calderawp\testing\resultDiff\ArrayLike\Form(
                 $this->getMockData()
             )
        );
    }
}