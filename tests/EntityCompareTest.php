<?php

/**
 * Class EntityCompareTest
 *
 * @covers \calderawp\testing\resultDiff\EntityCompare
 */
class EntityCompareTest extends CompareTestCase
{

    /**
     * Test that identical Entities are considered to have same form
     *
     * @covers \calderawp\testing\resultDiff\EntityCompare::validateForm()
     */
    public function testValidateWithValid()
    {

        $mock = new \calderawp\testing\resultDiff\DB\Entity(
            $this->mockData('Entity',
                [
                    'formId' => 'cf1234'
                ]
            )
        );
        $this->assertEquals( $mock, $mock );
        $this->assertTrue(
            ( new \calderawp\testing\resultDiff\EntityCompare( $mock, $mock ) )
            ->validateForm()
        );

    }

    /**
     * Test that non-identical Entities are considered to have same form
     *
     * @covers \calderawp\testing\resultDiff\EntityCompare::validateForm()
     */
    public function testValidateWithDifferentEntity()
    {
        $entityOne = new \calderawp\testing\resultDiff\DB\Entity(
            $this->mockData('Entity',  [
                'formId' => 'CF42'
            ])
        );

        $entityTwo = new \calderawp\testing\resultDiff\DB\Entity(
            $this->mockData('Entity',  [
                'formId' => 'CFhiRoy'
            ])
        );

        $comparer = new \calderawp\testing\resultDiff\EntityCompare( $entityOne, $entityTwo );


        $this->expectException( \Exception::class );

        $this->assertFalse( $comparer->validateForm() );
    }

    /**
     * Test that entities with different form IDs are not considered valid
     *
     * @covers \calderawp\testing\resultDiff\EntityCompare::validateForm()
     * @covers \calderawp\testing\resultDiff\EntityCompare::formIdEquals()
     */
    public function testValidateWithInvalidFormId()
    {
        $sharedArgs = [
            'formId' => 42,
        ];

        $entityOne = new \calderawp\testing\resultDiff\DB\Entity(
            $this->mockData('Entity',
                [
                    'formId' => 42,
                ]
            )
        );

        $entityTwo = new \calderawp\testing\resultDiff\DB\Entity(
            $this->mockData('Entity',
                [
                    'formId' => 43,
                ]
            )
        );

        $comparer = new \calderawp\testing\resultDiff\EntityCompare( $entityOne, $entityTwo );


        $this->expectException( \Exception::class );

        $this->assertFalse( $comparer->validateForm() );

    }




    /**
     * Test that different form hashes are considered invalid
     *
     * @covers \calderawp\testing\resultDiff\EntityCompare::validateForm()
     * @covers \calderawp\testing\resultDiff\EntityCompare::formEquals()
     */
    public function testValidateWithInvalidFormHash()
    {
        $entityOne = new \calderawp\testing\resultDiff\DB\Entity(
            $this->mockData('Entity',  [
                'formHash' => sha1('Roy' )
            ])
        );

        $entityTwo = new \calderawp\testing\resultDiff\DB\Entity(
            $this->mockData('Entity',  [
                'formHash' => sha1('Mike' )
            ])
        );

        $comparer = new \calderawp\testing\resultDiff\EntityCompare( $entityOne, $entityTwo );


        $this->expectException( \Exception::class );

        $this->assertFalse( $comparer->validateForm() );
    }

    /**
     * Test that two different hashes result in invalid result
     *
     * @covers \calderawp\testing\resultDiff\EntityCompare::validate();
     */
    public function testValidateWithInvalidHash()
    {


        $entityOne = new \calderawp\testing\resultDiff\DB\Entity(
            $this->mockData('Entity',  [
                'hash' => sha1( 'Roy' )
            ] )
        );

        $entityTwo = new \calderawp\testing\resultDiff\DB\Entity(
            $this->mockData('Entity',  [
                'hash' => sha1( 'Mike' )
            ] )
        );

        $comparer = new \calderawp\testing\resultDiff\EntityCompare( $entityOne, $entityTwo );

        $this->expectException( \Exception::class );

        $this->assertFalse( $comparer->validate() );
    }

    /**
     * @return \calderawp\testing\resultDiff\DB\Entity
     */
    protected function getMockEntity()
    {
        return new \calderawp\testing\resultDiff\DB\Entity(
            $this->mockData('Entity' )
        );
    }

}