<?php

/**
 * Class EntryCompareTest
 *
 * @covers \calderawp\testing\resultDiff\Comparisons\Entry
 */
class EntryCompareTest extends CompareTestCase
{

    /**
     * Test that mock data is valid
     *
     * @covers CompareTestCase::getMockEntryEntity()
     */
    public function testMockData()
    {
        $mockForm = $this->getMockForm();
        $entryEntity = $this->getMockEntryEntity();

        $this->assertInstanceOf(
            \calderawp\interop\Entities\Entry::class,
            $entryEntity
        );


    }

    /**
     *
     * @covers \calderawp\testing\resultDiff\Comparisons\Entry::prepareData()
     * @covers \calderawp\testing\resultDiff\Comparisons\Entry::toArray()
     */
    public function testPreparedData()
    {
        $mockForm = $this->getMockForm();
        $entryEntity = $this->getMockEntryEntity();

        $comparison = new \calderawp\testing\resultDiff\Comparisons\Entry(
            $entryEntity,
            $mockForm
        );

        $this->assertSame(
            $entryEntity->toArray(),
            $comparison->prepareData()
        );
    }

    /**
     * Test that two identical objects produce the same hash
     *
     * @covers \calderawp\testing\resultDiff\Comparisons\Entry::prepareData()
     * @covers \calderawp\testing\resultDiff\Comparisons\Entry::getHash()
     * @covers \calderawp\testing\resultDiff\Traits\HashesToArray::getHash()
     */
    public function testHashEquality()
    {
        $mockForm = $this->getMockForm();
        $entryEntity = $this->getMockEntryEntity();

        $comparisonOne = new \calderawp\testing\resultDiff\Comparisons\Entry(
            $entryEntity,
            $mockForm
        );

        $comparisonTwo = new \calderawp\testing\resultDiff\Comparisons\Entry(
            $entryEntity,
            $mockForm
        );

        $this->assertEquals(
            $comparisonOne->getHash(),
            $comparisonTwo->getHash()
        );
    }

    /**
     * Test that two non-identical objects do notproduce the same hash
     *
     * @covers \calderawp\testing\resultDiff\Comparisons\Entry::prepareData()
     * @covers \calderawp\testing\resultDiff\Comparisons\Entry::getHash()
     * @covers \calderawp\testing\resultDiff\Traits\HashesToArray::getHash()
     */
    public function testHashInequality()
    {
        $mockForm = $this->getMockForm();
        $entryEntity = $this->getMockEntryEntity();

        $comparisonOne = new \calderawp\testing\resultDiff\Comparisons\Entry(
            $entryEntity,
            $mockForm
        );

        $entryEntityTwo = $this->getMockEntryEntity();
        $entryEntityTwo->setID( rand( rand(50, 99 ), rand( 100, 200 )) );
        $comparisonTwo = new \calderawp\testing\resultDiff\Comparisons\Entry(
            $entryEntityTwo,
            $mockForm
        );

        $this->assertNotSame(
            $comparisonOne->getHash(),
            $comparisonTwo->getHash()
        );
    }

    /**
     * Test that type is 'Entry'
     *
     * @covers \calderawp\testing\resultDiff\Comparisons\Comparison::getType()
     * @covers \calderawp\testing\resultDiff\Comparisons\Entry::getType()
     */
    public function testGetType()
    {
        $mockForm = $this->getMockForm();
        $entryEntity = $this->getMockEntryEntity();

        $comparisonOne = new \calderawp\testing\resultDiff\Comparisons\Entry(
            $entryEntity,
            $mockForm
        );

        $this->assertEquals( 'Entry', $comparisonOne->getType() );
    }

    /**
     * Test entity conversion
     *
     * @covers \calderawp\testing\resultDiff\Comparisons\Comparison::toEntity()
     * @covers \calderawp\testing\resultDiff\Comparisons\Entry::toEntity()
     */
    public function testToEntity()
    {

        $mockForm = $this->getMockForm();
        $entryEntity = $this->getMockEntryEntity();
        $formComparison = new \calderawp\testing\resultDiff\Comparisons\Form( $mockForm );

        $comparison = new \calderawp\testing\resultDiff\Comparisons\Entry(
            $entryEntity,
            $mockForm
        );

        $mockEntityData = $this->mockData( 'Entity', [
            'formHash'  => $formComparison->getHash(),
            'formId'    => $mockForm[ 'ID' ],
            'type'      => 'Entry',
            'hash'      => $comparison->getHash()
        ]);

        $entity = new \calderawp\testing\resultDiff\DB\Entity(
            $mockEntityData
        );

        $entity->setID( $mockEntityData[ 'ID' ] );

        $resultEntity = $comparison->toEntity()->setID( $mockEntityData[ 'ID' ] );
        $this->assertEquals(
            $entity,
            $resultEntity

        );


    }

}